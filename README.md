In this code test you are given a CSV file with player stats.
Your assignment is two folded:

1. Create an backend that parses the CSV file and exposes it as a REST API.
2. Create a client that uses the REST API to fetch the data and display it in a nice
   way for the user.

You are free to choose technologies and architecture as you want. But an example
could be to use Spring Boot as a backend and then create a single page application
with React.

